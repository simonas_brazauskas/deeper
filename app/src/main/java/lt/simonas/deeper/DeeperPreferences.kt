package lt.simonas.deeper

import android.content.SharedPreferences
import androidx.core.content.edit

class DeeperPreferences(private val prefs: SharedPreferences) : SharedPreferences by prefs {


    var accessToken: String?
        get() = prefs.getString("accessToken", null)
        set(value) {
            prefs.edit {
                putString("accessToken", value)
            }
        }

    var currentUser: Long
        get() = prefs.getLong("user", -1)
        set(value) {
            prefs.edit {
                putLong("user", value)
            }
        }

    var refreshToken: String?
        get() = prefs.getString("refreshToken", null)
        set(value) {
            prefs.edit {
                putString("refreshToken", value)
            }
        }
}