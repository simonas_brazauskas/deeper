package lt.simonas.deeper.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import lt.simonas.deeper.MainActivity
import lt.simonas.deeper.di.scope.ActivityScope

@Module
abstract class ActivityModule {

    @Binds
    abstract fun bindViewModelFactory(factory: DeeperViewModelFactory): ViewModelProvider.Factory

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    @ActivityScope
    abstract fun contributeMainActivity(): MainActivity
}