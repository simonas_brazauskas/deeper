package lt.simonas.deeper.di.module

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import lt.simonas.deeper.BuildConfig
import lt.simonas.deeper.DeeperPreferences
import lt.simonas.deeper.api.AuthInterceptor
import lt.simonas.deeper.api.DeeperApi
import lt.simonas.deeper.api.RxErrorHandlingCallAdapterFactory
import lt.simonas.deeper.api.SchedulerProvider
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton


@Module
class ApplicationModule(private val app: Application) {


    @Singleton
    @Provides
    fun provideHttpClient(authInterceptor: AuthInterceptor): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(authInterceptor)
            .addInterceptor(logging)
            .build()
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return Gson()
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BuildConfig.SERVER_URL)
            .build()
    }

    @Singleton
    @Provides
    fun provideDeeperApi(retrofit: Retrofit): DeeperApi {
        return retrofit.create()
    }

    @Singleton
    @Provides
    fun provideSchedulerProvider(): SchedulerProvider {
        return SchedulerProvider.DEFAULT
    }

    @Singleton
    @Provides
    fun provideDeeperPreferences(): DeeperPreferences {
        return DeeperPreferences(app.getSharedPreferences("deeper", Context.MODE_PRIVATE))
    }
}