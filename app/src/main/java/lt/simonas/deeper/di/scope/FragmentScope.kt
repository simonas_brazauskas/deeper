package lt.simonas.deeper.di.scope

import javax.inject.Scope

@Scope
@Retention()
annotation class FragmentScope