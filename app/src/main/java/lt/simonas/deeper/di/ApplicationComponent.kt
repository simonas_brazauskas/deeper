package lt.simonas.deeper.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import lt.simonas.deeper.App
import lt.simonas.deeper.di.module.ApplicationModule
import lt.simonas.deeper.di.module.ActivityModule
import lt.simonas.deeper.di.module.DatabaseModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
        ApplicationModule::class,
        DatabaseModule::class,
        ActivityModule::class]
)
interface ApplicationComponent : AndroidInjector<DaggerApplication> {

    fun inject(app: App)
}