package lt.simonas.deeper.di.module

import dagger.Module
import dagger.Provides
import lt.simonas.deeper.database.DeeperDatabase
import javax.inject.Singleton

@Module
class DatabaseModule(private val deeperDatabase: DeeperDatabase) {

    @Singleton
    @Provides
    fun provideScanDao() = deeperDatabase.scanDao()
}