package lt.simonas.deeper.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import lt.simonas.deeper.di.scope.FragmentScope
import lt.simonas.deeper.fragment.home.HomeFragment
import lt.simonas.deeper.fragment.home.HomeModule
import lt.simonas.deeper.fragment.login.LoginFragment
import lt.simonas.deeper.fragment.login.LoginModule
import lt.simonas.deeper.fragment.map.MapFragment
import lt.simonas.deeper.fragment.map.MapModule

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector(modules = [LoginModule::class])
    @FragmentScope
    abstract fun LoginFragment(): LoginFragment

    @ContributesAndroidInjector(modules = [HomeModule::class])
    @FragmentScope
    abstract fun HomeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [MapModule::class])
    @FragmentScope
    abstract fun MapFragment(): MapFragment
}
