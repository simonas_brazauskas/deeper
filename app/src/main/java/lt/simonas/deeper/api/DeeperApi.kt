package lt.simonas.deeper.api

import io.reactivex.Single
import lt.simonas.deeper.api.response.LoginResponse
import lt.simonas.deeper.api.response.Polygon
import lt.simonas.deeper.api.response.Group
import retrofit2.http.GET
import retrofit2.http.Query

interface DeeperApi {

    @GET("login")
    fun login(@Query("username") userName: String, @Query("password") password: String): Single<LoginResponse>

    @GET("polygon")
    fun polygon(@Query("scanIds") scanIds: List<Long>): Single<Polygon>

    @GET("water/userGroups")
    fun userGroups(): Single<List<Group>>
}