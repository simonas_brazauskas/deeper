package lt.simonas.deeper.api.response

data class Group(
    val coverage: Long,
    val scanIds: List<Long>,
    val water: Water
)

data class Water(
    val area: Long,
    val bbox: List<List<Double>>,
    val center: List<Double>,
    val id: Long,
    val name: String?
)