package lt.simonas.deeper.api.exception

class UnknownApiErrorException(message: String, cause: Throwable) : Exception(message, cause)