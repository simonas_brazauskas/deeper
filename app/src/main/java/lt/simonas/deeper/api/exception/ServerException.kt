package lt.simonas.deeper.api.exception

import retrofit2.HttpException

class ServerException(cause: Throwable) : Exception(cause.message, cause)