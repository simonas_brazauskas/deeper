package lt.simonas.deeper.api

import io.reactivex.Single
import io.reactivex.SingleTransformer
import lt.simonas.deeper.DeeperPreferences
import lt.simonas.deeper.api.exception.AuthException
import lt.simonas.deeper.api.exception.TokenRefreshException
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class TokenRefreshInterceptor @Inject
constructor(
    private val deeperPreferences: DeeperPreferences,
    private val deeperAPI: DeeperApi,
    private val schedulerProvider: SchedulerProvider
) {

    fun <R> applyInterceptor(): SingleTransformer<R, R> {
        return SingleTransformer { observable ->
            observable.compose(applyDefer())
                .retryWhen { throwableObservable ->
                    throwableObservable.flatMapSingle { throwable ->
                        if (throwable is AuthException) {
                            val statusCode = throwable.statusCode
                            val authHeader = throwable.usedAuthHeader
                            Timber.d("Request failed with 401 status")
                            Timber.d("Auth header in request: %s", authHeader)
                            return@flatMapSingle when {
                                isTokenHasChanged(authHeader) -> {
                                    Timber.w("Token has changed!")
                                    Single.just(true)
                                }
                                deeperPreferences.refreshToken != null -> {
                                    TODO()
                                }
                                else -> {
                                    Timber.w("Token can not be refreshed")
                                    Single.error<Boolean>(
                                        TokenRefreshException(
                                            "No refresh token",
                                            throwable
                                        )
                                    )
                                }
                            }
                                .compose(schedulerProvider.applySchedulersForSingle())
                        }
                        Single.error<Throwable>(throwable)
                    }
                }
        }
    }

    private fun <R> applyDefer(): SingleTransformer<R, R> {
        return SingleTransformer { single -> Single.defer { single } }
    }

    private fun isTokenHasChanged(authHeader: String?): Boolean {
        val accessToken = deeperPreferences.accessToken
        Timber.d("Current access token: %s", accessToken)
        return authHeader != null && authHeader != "Bearer $accessToken"
    }

}
