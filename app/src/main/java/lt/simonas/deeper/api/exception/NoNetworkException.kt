package lt.simonas.deeper.api.exception

import retrofit2.HttpException

class NoNetworkException(cause: Throwable) : Exception(cause.message, cause)