package lt.simonas.deeper.api.exception

class TokenRefreshException(message: String, cause: Throwable) : Exception(message, cause)