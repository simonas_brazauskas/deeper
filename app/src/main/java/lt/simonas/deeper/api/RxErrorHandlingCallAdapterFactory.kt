package lt.simonas.deeper.api

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import lt.simonas.deeper.api.exception.*
import okhttp3.Request
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.RuntimeException
import java.lang.reflect.Type
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException


class RxErrorHandlingCallAdapterFactory private constructor() : CallAdapter.Factory() {

    private val original: RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<Call<*>, *>? {
        return RxCallAdapterWrapper(
            retrofit,
            convertToCallType(original.get(returnType, annotations, retrofit), CallAdapter::class.java)
        )
    }

    private class RxCallAdapterWrapper constructor(
        private val retrofit: Retrofit,
        private val wrapped: CallAdapter<Call<*>, Any>
    ) :
        CallAdapter<Call<*>, Any> {

        override fun responseType(): Type {
            return wrapped.responseType()
        }

        override fun adapt(call: Call<Call<*>>): Any {
            val adaptedCall = wrapped.adapt(call)

            if (adaptedCall is Completable) {
                return adaptedCall.onErrorResumeNext { throwable ->
                    Completable.error(
                        asRetrofitException(
                            throwable,
                            call.request()
                        )
                    )
                }
            }

            if (adaptedCall is Single<*>) {
                return adaptedCall.onErrorResumeNext { throwable ->
                    Single.error(
                        asRetrofitException(
                            throwable,
                            call.request()
                        )
                    )
                }
            }

            if (adaptedCall is Observable<*>) {
                return adaptedCall.onErrorResumeNext { throwable: Throwable ->
                    Observable.error(
                        asRetrofitException(
                            throwable,
                            call.request()
                        )
                    )
                }
            }

            throw RuntimeException("Observable Type not supported")
        }

        private fun asRetrofitException(throwable: Throwable, request: Request): Throwable {
            if (throwable is HttpException) {

                val rawPath = request.url().uri().rawPath
                val isLogin = rawPath.contains("login")

                val authHeader = throwable.response().raw().request().headers().get("Authorization")
                val statusCode = throwable.code()
                val response = throwable.response().errorBody()

                return try {
                    when {
                        isLogin || throwable.code() == 401 -> {
                            AuthException(statusCode, authHeader, throwable)
                        }
                        else -> {
                            ApiException(throwable)
                        }
                    }
                } catch (ioException: IOException) {
                    UnknownApiErrorException(String.format("Unknown API displayError: %d", statusCode), ioException)
                }


            } else if (throwable is SocketTimeoutException || throwable is ConnectException) {
                return ServerException(throwable)
            } else if (throwable is UnknownHostException) {
                return NoNetworkException(throwable)
            }
            return throwable
        }
    }

    companion object {

        fun create(): CallAdapter.Factory {
            return RxErrorHandlingCallAdapterFactory()
        }

        private fun convertToCallType(o: CallAdapter<*, *>?, clazz: Class<*>): CallAdapter<Call<*>, Any> {
            return clazz.cast(o) as CallAdapter<Call<*>, Any>
        }
    }

}
