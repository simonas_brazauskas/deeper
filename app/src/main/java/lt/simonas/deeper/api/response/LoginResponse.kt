package lt.simonas.deeper.api.response

data class LoginResponse(
    val login: Login,
    val user: User,
    val scans: List<Scan>
)

data class Login(
    val appId: String,
    val token: String,
    val userId: Long,
    val validated: Boolean,
    val validTill: String
)

data class User(
    val userId: Long,
    val familyName: String,
    val name: String,
    val email: String,
    val locale: String,
    val subscribe: Boolean
)

data class Scan(
    val id: Long,
    val name: String,
    val groupId: Long,
    val date: Long,
    val scanPoints: Long,
    val mode: Int
)
