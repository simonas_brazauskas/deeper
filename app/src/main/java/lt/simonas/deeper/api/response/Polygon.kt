package lt.simonas.deeper.api.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Polygon(
    val type: String,
    val bbox: List<Double>,
    val features: List<Feature>
) : Parcelable

@Parcelize
data class Feature(
    val type: String,
    val properties: Properties,
    val geometry:Geometry
) : Parcelable

@Parcelize
data class Properties(val depth: Double) : Parcelable

@Parcelize
data class Geometry(
    val type: String,
    val bbox: List<Double>,
    val coordinates: List<List<List<Double>>>
) : Parcelable