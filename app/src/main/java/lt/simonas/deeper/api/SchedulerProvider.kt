package lt.simonas.deeper.api

import io.reactivex.*
import io.reactivex.schedulers.Schedulers


interface SchedulerProvider {

    fun <T> applySchedulers(): ObservableTransformer<T, T>
    fun <T> applySchedulersFlowable(): FlowableTransformer<T, T>

    fun applySchedulersForCompletable(): CompletableTransformer
    fun <T> applySchedulersForSingle(): SingleTransformer<T, T>
    fun <T> applySchedulersForMaybe(): MaybeTransformer<T, T>

    companion object {

        val appScheduler = Schedulers.io()

        val DEFAULT: SchedulerProvider = object : SchedulerProvider {
            override fun <T> applySchedulers(): ObservableTransformer<T, T> {
                return ObservableTransformer { observable ->
                    observable.subscribeOn(appScheduler)
                }
            }

            override fun <T> applySchedulersFlowable(): FlowableTransformer<T, T> {
                return FlowableTransformer { flowable ->
                    flowable.subscribeOn(appScheduler)
                }
            }

            override fun applySchedulersForCompletable(): CompletableTransformer {
                return CompletableTransformer { completable ->
                    completable.subscribeOn(appScheduler)
                }
            }

            override fun <T> applySchedulersForSingle(): SingleTransformer<T, T> {
                return SingleTransformer { single ->
                    single.subscribeOn(appScheduler)
                }
            }

            override fun <T> applySchedulersForMaybe(): MaybeTransformer<T, T> {
                return MaybeTransformer { maybe ->
                    maybe.subscribeOn(appScheduler)
                }
            }
        }

        val MOCK: SchedulerProvider = object : SchedulerProvider {
            override fun <T> applySchedulers(): ObservableTransformer<T, T> {
                return ObservableTransformer { observable -> observable }
            }

            override fun <T> applySchedulersFlowable(): FlowableTransformer<T, T> {
                return FlowableTransformer { flowable -> flowable }
            }

            override fun applySchedulersForCompletable(): CompletableTransformer {
                return CompletableTransformer { observable -> observable }
            }

            override fun <T> applySchedulersForSingle(): SingleTransformer<T, T> {
                return SingleTransformer { single -> single }
            }

            override fun <T> applySchedulersForMaybe(): MaybeTransformer<T, T> {
                return MaybeTransformer { maybe -> maybe }
            }
        }
    }


}