package lt.simonas.deeper.api.exception

import retrofit2.HttpException

class AuthException(
    val statusCode: Int,
    val usedAuthHeader: String?,
    cause: HttpException
) : Exception(cause.message(), cause)