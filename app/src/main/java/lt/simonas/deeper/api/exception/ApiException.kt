package lt.simonas.deeper.api.exception

import retrofit2.HttpException

class ApiException(cause: HttpException) : Exception(cause.message(), cause)