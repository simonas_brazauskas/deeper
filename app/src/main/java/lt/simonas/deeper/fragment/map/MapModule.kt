package lt.simonas.deeper.fragment.map

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import lt.simonas.deeper.di.ViewModelKey

@Module
abstract class MapModule {
    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    abstract fun MapViewModel(viewModel: MapViewModel): ViewModel
}