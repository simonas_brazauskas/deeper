package lt.simonas.deeper.fragment.login

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import lt.simonas.deeper.di.ViewModelKey

@Module
abstract class LoginModule{

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun LoginViewModel(viewModel: LoginViewModel): ViewModel


}