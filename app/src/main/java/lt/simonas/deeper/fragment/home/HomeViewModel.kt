package lt.simonas.deeper.fragment.home

import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import lt.simonas.deeper.base.BaseViewModel
import lt.simonas.deeper.database.entity.ScanEntity
import lt.simonas.deeper.di.scope.FragmentScope
import timber.log.Timber
import javax.inject.Inject

@FragmentScope
class HomeViewModel @Inject constructor(private val homeUseCase: HomeUseCase) : BaseViewModel() {

    val scans = MutableLiveData<List<ScanEntity>>()
    val loading = MutableLiveData<Boolean>()

  init {
      disposables += homeUseCase.getScans()
          .observeOn(AndroidSchedulers.mainThread())
          .doOnSubscribe {
              loading.value = true
          }
          .subscribe({ entities ->
              scans.value = entities
              loading.value = false
          }, {
              loading.value = false
              Timber.e(it)
          })
  }
}