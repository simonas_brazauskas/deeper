package lt.simonas.deeper.fragment.map

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolygonOptions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import lt.simonas.deeper.base.BaseViewModel
import lt.simonas.deeper.database.entity.ScanEntity
import lt.simonas.deeper.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class MapViewModel @Inject constructor(private val mapUseCase: MapUseCase) : BaseViewModel() {


    val polygons = MutableLiveData<List<PolygonOptions>>()
    val bbox = MutableLiveData<LatLngBounds>()

    fun mapReady(scanEntity: ScanEntity) {
        disposables += mapUseCase.getPolygons(scanEntity)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { polys ->
                polygons.value = polys
            }
        disposables += mapUseCase.getBbox(scanEntity)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { bounds ->
                bbox.value = bounds
            }

    }
}

data class Polygon(
    val coordinates: List<LatLng>,
    val depth: Double
)