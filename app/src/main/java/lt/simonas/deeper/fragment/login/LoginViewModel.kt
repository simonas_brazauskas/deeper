package lt.simonas.deeper.fragment.login

import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import lt.simonas.deeper.R
import lt.simonas.deeper.base.BaseViewModel
import lt.simonas.deeper.di.scope.FragmentScope
import lt.simonas.deeper.util.SingleLiveEvent
import timber.log.Timber
import javax.inject.Inject

@FragmentScope
class LoginViewModel @Inject constructor(private val loginUseCase: LoginUseCase) : BaseViewModel() {


    val navigationEvent = SingleLiveEvent<Int>()

    val error = MutableLiveData<Throwable?>()

    val progress = MutableLiveData<Boolean>()

    fun login(username: String, password: String) {
        disposables += loginUseCase.login(username, password)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                progress.value = true
            }
            .subscribe({
                navigationEvent.value = R.id.action_loginFragment_to_homeFragment
                progress.value = false
            }, {
                Timber.e(it)
                progress.value = false
                error.value = it
            })
    }

    fun clearError() {
        error.value = null
    }
}