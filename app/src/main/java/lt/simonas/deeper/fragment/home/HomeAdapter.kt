package lt.simonas.deeper.fragment.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import lt.simonas.deeper.R
import lt.simonas.deeper.database.entity.ScanEntity
import java.util.*


class HomeAdapter : ListAdapter<ScanEntity, HomeAdapter.ScanViewHolder>(ScanEntityDiffItemCallback()) {

    var onClickListener: ((scanEntity: ScanEntity) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScanViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_scan, parent, false)
        return ScanViewHolder(view)
    }

    override fun onBindViewHolder(holder: ScanViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }

    inner class ScanViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val scanName = itemView.findViewById<TextView>(R.id.scan_name)
        private val scanDate = itemView.findViewById<TextView>(R.id.scan_date)
        fun bind(item: ScanEntity) {
            itemView.setOnClickListener { onClickListener?.invoke(item) }
            scanName.text = item.name
            val date = Date(item.date)
            scanDate.text = date.toString()
        }
    }
}

private class ScanEntityDiffItemCallback : DiffUtil.ItemCallback<ScanEntity>() {
    override fun areItemsTheSame(oldItem: ScanEntity, newItem: ScanEntity): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ScanEntity, newItem: ScanEntity): Boolean {
        return oldItem.hashCode() == newItem.hashCode()
    }
}