package lt.simonas.deeper.fragment.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_home.*
import lt.simonas.deeper.R
import lt.simonas.deeper.base.BaseFragment
import javax.inject.Inject
import androidx.recyclerview.widget.DividerItemDecoration


class HomeFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val adapter = HomeAdapter().apply {
        setHasStableIds(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val manager = LinearLayoutManager(requireContext())
        home_list.layoutManager = manager
        home_list.setHasFixedSize(true)
        home_list.adapter = adapter
        val vm = ViewModelProviders.of(this, viewModelFactory)[HomeViewModel::class.java]

        vm.scans.observe(this, Observer { entities ->
            adapter.submitList(entities)
        })
        vm.loading.observe(this, Observer { isLoading ->
            home_progress.isVisible = isLoading
            home_list.isVisible = !isLoading
        })
        val dividerItemDecoration = DividerItemDecoration(
            requireContext(),
            manager.orientation
        )
        home_list.addItemDecoration(dividerItemDecoration)
        adapter.onClickListener = { scanEntity ->
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToMapFragment(scanEntity))
        }
    }

}