package lt.simonas.deeper.fragment.home

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import lt.simonas.deeper.di.ViewModelKey

@Module
abstract class HomeModule{
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun HomeViewModel(viewModel: HomeViewModel): ViewModel

}