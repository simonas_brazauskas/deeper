package lt.simonas.deeper.fragment.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import lt.simonas.deeper.R
import lt.simonas.deeper.base.BaseFragment
import javax.inject.Inject


class MapFragment : BaseFragment(), OnMapReadyCallback {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val args by navArgs<MapFragmentArgs>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(map: GoogleMap) {
        val vm = ViewModelProviders.of(this, viewModelFactory)[MapViewModel::class.java]
        vm.mapReady(args.scanEntity)
        vm.polygons.observe(this, Observer { polygons ->
            map.clear()
            polygons.forEach {
                map.addPolygon(it)
            }
        })
        vm.bbox.observe(this, Observer {
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(it, 200))

        })
    }
}