package lt.simonas.deeper.fragment.home

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import lt.simonas.deeper.DeeperPreferences
import lt.simonas.deeper.api.DeeperApi
import lt.simonas.deeper.api.SchedulerProvider
import lt.simonas.deeper.base.BaseUseCase
import lt.simonas.deeper.database.ScanDao
import lt.simonas.deeper.database.entity.ScanEntity
import lt.simonas.deeper.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class HomeUseCase @Inject constructor(
    private val scanDao: ScanDao,
    private val deeperPreferences: DeeperPreferences,
    schedulerProvider: SchedulerProvider
) :
    BaseUseCase(schedulerProvider) {

    fun getScans(): Observable<List<ScanEntity>> {
        if (deeperPreferences.currentUser == -1L)
            return Observable.empty()
        return scanDao.get(deeperPreferences.currentUser)
            .toObservable()
            .compose(schedulerProvider.applySchedulers())
    }

}