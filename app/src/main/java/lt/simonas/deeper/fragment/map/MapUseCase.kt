package lt.simonas.deeper.fragment.map

import android.graphics.Color
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolygonOptions
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_maps.*
import lt.simonas.deeper.api.SchedulerProvider
import lt.simonas.deeper.base.BaseUseCase
import lt.simonas.deeper.database.entity.ScanEntity
import lt.simonas.deeper.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class MapUseCase @Inject constructor(schedulerProvider: SchedulerProvider) : BaseUseCase(schedulerProvider) {
    fun getPolygons(scanEntity: ScanEntity): Single<List<PolygonOptions>> {
        return Single.fromCallable {
            scanEntity.polygon.features
                .flatMap { feature ->
                    feature.geometry.coordinates.map { outside ->
                        val coordinates = outside.map { inside -> LatLng(inside[1], inside[0]) }
                        Polygon(coordinates, feature.properties.depth)
                    }
                }
                .map { polygon ->
                    PolygonOptions()
                        .add(*polygon.coordinates.toTypedArray())
                        .strokeColor(Color.GRAY)
                        .strokeWidth(1f)
                        .fillColor(getColor(polygon.depth))
                }
        }
            .compose(schedulerProvider.applySchedulersForSingle())
    }

    fun getBbox(scanEntity: ScanEntity): Single<LatLngBounds> {
        return Single.fromCallable {
            val bbox = scanEntity.polygon.bbox
            LatLngBounds.builder()
                .include(LatLng(bbox[0], bbox[1]))
                .include(LatLng(bbox[2], bbox[3]))
                .build()

        }
            .compose(schedulerProvider.applySchedulersForSingle())
    }


    private fun getColor(depth: Double): Int {
        return when (depth) {
            in 0.0..0.6 -> Color.rgb(254, 127, 0)
            in 0.6..1.0 -> Color.rgb(253, 141, 0)
            in 1.0..2.0 -> Color.rgb(251, 172, 0)
            in 2.0..3.0 -> Color.rgb(249, 197, 0)
            in 3.0..4.0 -> Color.rgb(247, 216, 0)
            in 4.0..5.0 -> Color.rgb(245, 231, 0)
            in 5.0..6.0 -> Color.rgb(242, 243, 0)
            in 6.0..8.0 -> Color.rgb(207, 239, 0)
            in 8.0..10.0 -> Color.rgb(164, 235, 0)
            in 10.0..12.0 -> Color.rgb(105, 231, 0)
            in 12.0..16.0 -> Color.rgb(0, 220, 57)
            in 16.0..20.0 -> Color.rgb(0, 195, 226)
            in 20.0..25.0 -> Color.rgb(0, 175, 219)
            in 25.0..30.0 -> Color.rgb(3, 156, 212)
            in 30.0..35.0 -> Color.rgb(5, 129, 196)
            in 35.0..40.0 -> Color.rgb(0, 103, 180)
            in 40.0..50.0 -> Color.rgb(0, 61, 152)
            in 50.0..60.0 -> Color.rgb(0, 9, 137)
            in 60.0..70.0 -> Color.rgb(32, 0, 121)
            else -> Color.rgb(64, 0, 106)

        }
    }
}