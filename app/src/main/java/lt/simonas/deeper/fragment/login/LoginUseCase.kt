package lt.simonas.deeper.fragment.login

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import lt.simonas.deeper.DeeperPreferences
import lt.simonas.deeper.api.DeeperApi
import lt.simonas.deeper.api.SchedulerProvider
import lt.simonas.deeper.api.response.Polygon
import lt.simonas.deeper.api.response.Scan
import lt.simonas.deeper.base.BaseUseCase
import lt.simonas.deeper.database.ScanDao
import lt.simonas.deeper.database.entity.ScanEntity
import lt.simonas.deeper.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class LoginUseCase @Inject constructor(
    private val deeperApi: DeeperApi,
    private val scanDao: ScanDao,
    private val deeperPreferences: DeeperPreferences,
    schedulerProvider: SchedulerProvider
) : BaseUseCase(schedulerProvider) {

    fun login(userName: String, password: String): Completable {
        return deeperApi.login(userName, password)
            .doOnSuccess { response ->
                deeperPreferences.accessToken = response.login.token
                deeperPreferences.currentUser = response.login.userId
            }
            .flatMapCompletable { response ->
                deeperApi.userGroups()
                    .flatMapCompletable { groups ->
                        Observable.fromIterable(groups.filter { it.water.name != null })
                            .flatMapCompletable { group ->
                                polygon(group.scanIds)
                                    .flatMapCompletable { polygon ->
                                        scanDao.insert(
                                            listOf(
                                                ScanEntity(
                                                    group.water.id,
                                                    response.user.userId,
                                                    group.water.name?:"No name",
                                                    System.currentTimeMillis(),
                                                    polygon
                                                )
                                            )
                                        )
                                    }
                            }
                    }
            }
            .compose(schedulerProvider.applySchedulersForCompletable())
    }

    private fun polygon(scanIds: List<Long>): Single<Polygon> {
        return deeperApi.polygon(scanIds)
            .compose(schedulerProvider.applySchedulersForSingle())
    }
}