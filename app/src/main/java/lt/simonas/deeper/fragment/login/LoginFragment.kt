package lt.simonas.deeper.fragment.login

import android.app.ProgressDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_login.*
import lt.simonas.deeper.R
import lt.simonas.deeper.api.exception.AuthException
import lt.simonas.deeper.base.BaseFragment
import javax.inject.Inject

class LoginFragment : BaseFragment(), TextWatcher {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var progressDialog: ProgressDialog? = null

    private var alertDialog: AlertDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val vm = ViewModelProviders.of(this, viewModelFactory)[LoginViewModel::class.java]
        login_button.setOnClickListener {
            vm.login(login_username.text.toString(), login_password.text.toString())
        }
        login_username.addTextChangedListener(this)
        login_password.addTextChangedListener(this)

        vm.navigationEvent.observe(this, Observer {
            findNavController().navigate(it)
        })

        vm.progress.observe(this, Observer { showProgress ->
            if (showProgress) {
                progressDialog =
                    ProgressDialog.show(requireActivity(), null, getString(R.string.loging_in), true, false)
            } else {
                progressDialog?.dismiss()
            }
        })
        vm.error.observe(this, Observer { error ->
            if (error != null) {
                val message = if (error is AuthException) {
                    getString(R.string.error_auth)
                } else {
                    error.message
                }
                alertDialog = AlertDialog.Builder(requireActivity())
                    .setMessage(message)
                    .setPositiveButton(R.string.ok) { dialog, which ->
                        vm.clearError()
                    }
                    .show()
            }
        })
        login_button.isEnabled = false
        login_username.setText("deeperangler@gmail.com")
        login_password.setText("Deeper10899")
    }

    override fun afterTextChanged(s: Editable?) {
        val userName = login_username.text
        val password = login_password.text
        login_button.isEnabled = !(userName.isNullOrEmpty() || password.isNullOrEmpty())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

}