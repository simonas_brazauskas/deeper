package lt.simonas.deeper

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.room.Room
import androidx.room.RoomDatabase
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector
import lt.simonas.deeper.database.DeeperDatabase
import lt.simonas.deeper.di.ApplicationComponent
import lt.simonas.deeper.di.DaggerApplicationComponent
import lt.simonas.deeper.di.Injectable
import lt.simonas.deeper.di.module.ApplicationModule
import lt.simonas.deeper.di.module.DatabaseModule


object AppInjector {

    lateinit var component: ApplicationComponent
    fun init(app: App) {

        val database = Room.databaseBuilder(app, DeeperDatabase::class.java, "deeper_database")
            .fallbackToDestructiveMigration()
            .build()

        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(app))
            .databaseModule(DatabaseModule(database))
            .build()

        component.inject(app)

        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                handleActivity(activity)
            }

            override fun onActivityStarted(activity: Activity) {

            }

            override fun onActivityResumed(activity: Activity) {

            }

            override fun onActivityPaused(activity: Activity) {

            }

            override fun onActivityStopped(activity: Activity) {

            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {

            }

            override fun onActivityDestroyed(activity: Activity) {

            }
        })
    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                .registerFragmentLifecycleCallbacks(
                    object : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentCreated(
                            fm: FragmentManager,
                            f: Fragment,
                            savedInstanceState: Bundle?
                        ) {
                            if (f is Injectable) {
                                AndroidSupportInjection.inject(f)
                            }
                        }
                    }, true
                )
        }
    }
}