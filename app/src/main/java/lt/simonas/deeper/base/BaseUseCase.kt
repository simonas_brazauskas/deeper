package lt.simonas.deeper.base

import lt.simonas.deeper.api.AuthInterceptor
import lt.simonas.deeper.api.SchedulerProvider

abstract class BaseUseCase(val schedulerProvider: SchedulerProvider)