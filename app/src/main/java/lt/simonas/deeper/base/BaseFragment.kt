package lt.simonas.deeper.base

import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.*
import lt.simonas.deeper.di.Injectable

abstract class BaseFragment : Fragment(), Injectable {

    override fun onDestroyView() {
        super.onDestroyView()
        clearFindViewByIdCache()
    }
}