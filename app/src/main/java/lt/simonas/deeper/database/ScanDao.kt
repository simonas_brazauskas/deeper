package lt.simonas.deeper.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable
import lt.simonas.deeper.database.entity.ScanEntity

@Dao
interface ScanDao {

    @Query("SELECT * FROM ScanEntity WHERE userId =:userId")
    fun get(userId: Long): Flowable<List<ScanEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(scans: List<ScanEntity>): Completable
}