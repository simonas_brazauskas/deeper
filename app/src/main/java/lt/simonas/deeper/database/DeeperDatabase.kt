package lt.simonas.deeper.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import lt.simonas.deeper.database.entity.ScanEntity

@Database(
    entities = [ScanEntity::class],
    version =4,
    exportSchema = false
)
@TypeConverters(PolygonConveter::class)
abstract class DeeperDatabase : RoomDatabase() {
    abstract fun scanDao(): ScanDao
}