package lt.simonas.deeper.database.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import lt.simonas.deeper.api.response.Polygon

@Entity
@Parcelize
data class ScanEntity(
    @PrimaryKey(autoGenerate = false) val id: Long,
    val userId: Long,
    val name: String,
    val date: Long,
    val polygon: Polygon
) : Parcelable