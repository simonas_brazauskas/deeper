package lt.simonas.deeper.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import lt.simonas.deeper.api.response.Polygon

class PolygonConveter {

    private val gson = Gson()

    @TypeConverter
    fun fromDbValue(value: String?): Polygon? {
        return if (value == null) null else gson.fromJson(value, Polygon::class.java)
    }

    @TypeConverter
    fun toDBValue(value: Polygon?): String? {
        if (value == null) return null
        return gson.toJson(value)
    }
}